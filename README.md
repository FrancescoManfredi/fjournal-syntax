# FJournal syntax
In the years I've developed my own way of writing daily notes and todo-lists.
This is a syntax file to make Sublime Text 3 understand and highlight the
various parts of my journal files.

## How to install
Download this project directory and put it in your .config/sublime-text-3/Packages/
directory.

Set your sublime text color scheme to EditedMonokai. The color scheme file contained
in this project is basically the same as Monokai with the addition of some rules
(ending in .fj) to apply colors to this syntax.

## Syntax

### Titles

#### Main titles
First level titles are identified by text surrounded by 5 dashes on each side:  
`----- Main Title -----`  

#### Second level titles
Two dashes before and after:  
`-- Second Level Title --`

#### Lower Level Titles
A line all written in uppercase and ending with colon is a lower level title:  
`LOWER LEVEL TITLE:`  

### List items

List items are used to mark things to do, done, almost done, postponed and
already done (programmed for today but done in a previous day).  
Every item ends with an empty line, so items need to be separated by empty
lines. This allows to write longer items on multiple lines.  
You can start item lines with spaces or tabs and you will be able to obtain
different highlighting styles on each line, so you can make groups of items
for complex activities.  

#### TO DO
A minus sign identifies things that need to be done:  
`- Pet my dog for 15 minutes  `

#### DONE
`+ Pet my dog for 23h 45m  `

#### ALMOST DONE
`~ Pet my dog for 24h  `

#### POSTPONED
`-> Pet my dog for 24 more hours  
    sadly days only have 24h...  `

#### ALREADY DONE
`<- Pet my dog for the first time  `

### Other

#### Pasted
Pasted text che be surrounded by backticks (\`)  

#### Optional
Optional elements can be presented in square brackets paying attention to
have spaces between the content and the brackets:  
`[ This is an optional sentence ]`